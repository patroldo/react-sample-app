/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Router, Scene, ActionConst } from 'react-native-router-flux';
import { BackHandler } from 'react-native';
import { Alert } from 'react-native';
import Home from './src/components/home/Home'
import Main from './src/components/main/Main'
import Camera from './src/components/camera/Camera'
import ToDoList from './src/components/todolist/ToDoList'
import SceneAnimation from './src/components/other/SceneAnimation'
import BuyPage from './src/components/buytocall/BuyPage'
import CallPage from './src/components/call/CallPage'

global.PaymentRequest = require('react-native-payments').PaymentRequest;

type Props = {};
export default class App extends Component<Props> {

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    Alert.alert(
      'Confirm exit',
      'Do you want to quit the app?'
      [
        { text: 'CANCEL', style: 'cancel' },
        { text: 'OK', onPress: () => BackHandler.exitApp() }
      ]
    );
    return true;
  }

  transitionConfig = () => ({
    screenInterpolator: (props) => {
      switch (props.scene.route.params.direction) {
        case 'verticalFromTop':
          return SceneAnimation.forVerticalFromTop(props);
        default:
          return SceneAnimation.forHorizontalFromRight(props);
      }
    }
  })

  render() {
    return (
      <Router>
        <Scene key="root" transitionConfig={this.transitionConfig}>
          <Scene key="home"
            component={Home}
            hideNavBar
          />
          <Scene
            key="main"
            component={Main}
            hideNavBar
          />
          <Scene
            key="camera"
            hideNavBar
            component={Camera}
          />
          <Scene
            key="todolist"
            component={ToDoList}
            hideNavBar
            direction='verticalFromTop'
          />
          <Scene
            key="buypage"
            component={BuyPage}
            hideNavBar
            direction='verticalFromTop'
          />
          <Scene
            key="buypage"
            component={CallPage}
            hideNavBar
            initial
            direction='verticalFromTop'
          />
        </Scene>
      </Router>

    );

  }
}

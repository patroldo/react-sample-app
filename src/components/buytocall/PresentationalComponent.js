import React from 'react';
import { StyleSheet, Dimensions, View, Text } from 'react-native';
import NavBar from './NavBar';

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

export default class PresentationalComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.containerNavBar}>
                    <NavBar />
                </View>
                <View>
                    <Text>Buy me!!!</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        zIndex: 0
    },
    containerNavBar: {
        position: 'absolute',
        top: 0,
        zIndex: 1,
        width: '100%'
    }
});
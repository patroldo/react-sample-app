import React, { Component } from 'react';
import PresentationalComponent from './PresentationalComponent';

class BuyPage extends Component {

    METHOD_DATA = [{
        supportedMethods: ['android-pay'],
        data: {
            merchantIdentifier: 'merchant.com.your-app.namespace',
            supportedNetworks: ['visa', 'mastercard', 'amex'],
            countryCode: 'UA',
            currencyCode: 'USD',
            paymentMethodTokenizationParameters: {
                tokenizationType: 'PAYMENT_GATEWAY',
                     parameters: {
                       publicKey: 'BMiHUwusJ91zsBk6CX4Vgt5IlEAHLs+fW06IB652VBenqek4nPFb3IjQaZ+3sBBCJ436ELzpiZ0IrK8SEmvVq2c=',
                       gateway: 'stripe',
                       'stripe:publishableKey': 'pk_test_SqSlsjsKtwICcs3pS3DBqb8p',
                       'stripe:version': '5.0.0' // Only required on Android
                     }
                   }
        }
    }];

    DETAILS = {
        id: 'basic-example',
        displayItems: [
            {
                label: 'Movie Ticket',
                amount: { currency: 'USD', value: '15.00' }
            }
        ],
        total: {
            label: 'Merchant Name',
            amount: { currency: 'USD', value: '15.00' }
        }
    };

    paymentRequest = new PaymentRequest(this.METHOD_DATA, this.DETAILS);
    
    state = {
    }
    
    componentDidMount() {
        this.paymentRequest.show();
    }

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <PresentationalComponent
            ></PresentationalComponent>
        );
    }
}

export default BuyPage;
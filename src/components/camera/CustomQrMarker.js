import React from 'react';
import { StyleSheet, View, Dimensions, Animated } from 'react-native';


const lineWeight = 4;//line weight of green line
const backGroundColorCamera = "rgba(150,64,255,0.4)";//background color of outdoor marker
const marginOfBorderMarkers = - 10;// margin of each small rectangles in direction of appropriate corner
const rectanglesWidth = 60;// this is descripe the width small rectangles, which is used to draw lines on marker
const rectanglesHeight = 60;// this is descripe the height small rectangles, which is used to draw lines on marker
const bigRectangleWidth = 230;// this is descripe the width BIG rectangle, which is used to positioning small rectangles
const bigRectangleHeight = 230;// this is descripe the width BIG rectangles, which is used to positioning small rectangles



export default class CustomQrMarker extends React.Component {

    styles = eee(this.props.width, this.props.height);

    render() {
        return (
            <View style={this.styles.rectangleContainer}>
                <View style={this.styles.bottomHorizontalOutMarker} />
                <View style={this.styles.bottomVerticalOutMarker} />
                <View style={this.styles.topVerticalOutMarker} />
                <View style={this.styles.topHorizontalOutMarker} />

                <View style={this.styles.rectangle}>
                    <View style={this.styles.bottomRight} />
                    <View style={this.styles.bottomLeft} />
                    <View style={this.styles.topRight} />
                    <View style={this.styles.topLeft} />

                    <Animated.View style={[
                        this.styles.Animatedline,
                        {
                            top: this.props.fadeAnim,
                            transform: [{
                                translateY: this.props.fadeAnim.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [0, bigRectangleHeight - 3]
                                    // 0 : 4, 0.5 : (242-4)/2, 1 : 242
                                }),
                            }]
                        },
                    ]}></Animated.View>
                </View>
            </View >
        );
    }

    
}


function eee(SCREEN_WIDTH, SCREEN_HEIGHT) {
    return StyleSheet.create({
        rectangleContainer: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'transparent',
            width: "100%"
        },
        rectangle: {
            height: bigRectangleHeight,
            width: bigRectangleWidth,
            backgroundColor: 'transparent',
            shadowOffset: { width: 0, height: 0 },
            shadowColor: 'rgba(44,179,209,1.0)',
            shadowOpacity: 0.5,
            shadowRadius: 50,
            zIndex: 3
        },
        topLeft: {
            position: 'absolute',
            top: marginOfBorderMarkers,
            left: marginOfBorderMarkers,
            height: rectanglesHeight,
            width: rectanglesWidth,
            borderTopWidth: 2,
            borderLeftWidth: 2,
            borderColor: '#d6d7da',
            backgroundColor: 'transparent',
        },
        bottomLeft: {
            position: 'absolute',
            bottom: marginOfBorderMarkers,
            left: marginOfBorderMarkers,
            height: rectanglesHeight,
            width: rectanglesWidth,
            borderBottomWidth: 2,
            borderLeftWidth: 2,
            borderColor: '#d6d7da',
            backgroundColor: 'transparent',
        },
        bottomRight: {
            position: 'absolute',
            bottom: marginOfBorderMarkers,
            right: marginOfBorderMarkers,
            height: rectanglesHeight,
            width: rectanglesWidth,
            borderBottomWidth: 2,
            borderRightWidth: 2,
            borderColor: '#d6d7da',
            backgroundColor: 'transparent',
        },
        topRight: {
            position: 'absolute',
            top: marginOfBorderMarkers,
            right: marginOfBorderMarkers,
            height: rectanglesHeight,
            width: rectanglesWidth,
            borderTopWidth: 2,
            borderRightWidth: 2,
            borderColor: '#d6d7da',
            backgroundColor: 'transparent',
        },
        Animatedline: {
            width: bigRectangleWidth,
            position: 'absolute',
            left: 0,
            borderColor: 'green',
            borderBottomWidth: lineWeight
        },
        topHorizontalOutMarker: {//top left horizontal rectangle background out of marker
            top: 0,
            left: 0,
            position: 'absolute',
            width: SCREEN_WIDTH - ((SCREEN_WIDTH - bigRectangleWidth) / 2),
            height: (SCREEN_HEIGHT - bigRectangleHeight) / 2,
            backgroundColor: backGroundColorCamera
        },
        topVerticalOutMarker: {//top right vertical rectangle background out of marker
            top: 0,
            right: 0,
            position: 'absolute',
            width: (SCREEN_WIDTH - bigRectangleWidth) / 2,
            height: SCREEN_HEIGHT - ((SCREEN_HEIGHT - bigRectangleHeight) / 2),
            backgroundColor: backGroundColorCamera
        },
        bottomHorizontalOutMarker: {//bottom right horizontal(to right) rectangle background out of marker
            bottom: 0,
            right: 0,
            position: 'absolute',
            width: SCREEN_WIDTH - ((SCREEN_WIDTH - bigRectangleWidth) / 2),
            height: (SCREEN_HEIGHT - bigRectangleHeight) / 2,
            backgroundColor: backGroundColorCamera
        },
        bottomVerticalOutMarker: {//bottom left vertical(to top) rectangle background out of marker
            bottom: 0,
            left: 0,
            position: 'absolute',
            width: (SCREEN_WIDTH - bigRectangleWidth) / 2,
            height: SCREEN_HEIGHT - ((SCREEN_HEIGHT - bigRectangleHeight) / 2),
            backgroundColor: backGroundColorCamera
        }
    });
}
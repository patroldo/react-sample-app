import React, { Component } from 'react';
import PresentationalComponent from './PresentationalComponent';
import { Actions } from 'react-native-router-flux';
import { Main } from '../main/Main'

class Camera extends Component {
    state = {
        isAllowScan: true
    }

    changeInput(e, reenableScan) {        

        Actions.todolist({
            allowScanning: ()=> {
                this.refs.view.scanner.reactivate();                
            }
        });
    }

    render() {
        return (
                <PresentationalComponent
                    ref = 'view'
                    read = {this.changeInput.bind(this)}
                ></PresentationalComponent>
        );
    }
}

export default Camera;
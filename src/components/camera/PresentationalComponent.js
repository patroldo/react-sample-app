import React, { Component } from 'react';
import { StyleSheet, Text, View, Dimensions, Animated } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import CustomQrMarker from './CustomQrMarker';

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;
const textPadding = 40;//this describe padding of text in pop-up
const popupMoveDuration = 300;
const greenLineMoveDuration = 750;//this is describe the duration of animation(pop-up quicker on 700 ms)

class PresentationalComponent extends Component {

    state = {
        lineMoving: new Animated.Value(0),  // Initial value for opacity: 0
        textShowing: new Animated.Value(0),  // Initial value for opacity: 0
    }

    startScanningAnim(e) {
        Animated.parallel([
            Animated.timing(                  // Animate over time
                this.state.lineMoving,            // The animated value to drive the line
                {
                    toValue: 1,
                    duration: greenLineMoveDuration,
                }
            ),
            Animated.timing(
                this.state.textShowing,            // The animated value to drive a popup
                {
                    toValue: 1,
                    duration: popupMoveDuration,
                })
        ]).start(() => {
            this.finishScanningAnim(e);
        });                        // Starts the animation
    }

    finishScanningAnim(e) {
        Animated.timing(                  // Animate over time
            this.state.lineMoving,            // The animated value to drive the line
            {
                toValue: 0,
                duration: greenLineMoveDuration,
            }
        ).start(() => {
            this.props.read(e);
            this.closePopUp()
        });
    }

    closePopUp() {
        Animated.timing(
            this.state.textShowing,            // The animated value to drive the popup
            {
                toValue: 0,
                duration: 1,
            })
            .start();
    }

    render() {
        return (
            <View>
                <QRCodeScanner
                    fadeIn={true}
                    onRead={(e) => {
                        this.startScanningAnim(e);
                    }}
                    cameraStyle={styles.cameraStyle}
                    showMarker={true}
                    customMarker={
                        <CustomQrMarker
                            fadeAnim={this.state.lineMoving}
                            width={SCREEN_WIDTH}
                            height={SCREEN_HEIGHT} />
                    }
                    ref={(node) => { this.scanner = node }}
                    topContent={
                        <Animated.View style={[
                            {
                                top: this.state.textShowing,
                                transform: [{
                                    translateY: this.state.textShowing.interpolate({
                                        inputRange: [0, 1],
                                        outputRange: [-100, 0]
                                        // 0 : 4, 0.5 : (242-4)/2, 1 : 242
                                    }),
                                }]
                            },
                            styles.scanning]}>
                            <Text style={styles.textScan}>Scanning your coupon QR code</Text>
                        </Animated.View>
                    }
                />
            </View>
        )
    };
}

const styles = StyleSheet.create({
    scanning: {
        display: 'flex',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: textPadding,
        paddingBottom: textPadding,
        backgroundColor: 'white',
        position: 'absolute',
        zIndex: 2,
    },
    cameraStyle: {
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT
    },
    textScan: {
        fontSize: 16
    }
});

export default PresentationalComponent;
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import PresentationalComponent from './PresentationalComponent';

class Main extends Component {
    state = {
        //phoneNumber: this.props.phoneNumber,
        //nickname: this.props.nickname
        nickname: "aaa",
        phoneNumber: "aaa",
        moneys: "aaa"
    };

    goToCamera() {
        Actions.jump('camera',{phoneNumber: this.state.phoneNumber});
    }



    render() {
        return (
            <PresentationalComponent
                goToCamera={this.goToCamera.bind(this)}
                nickname={this.state.nickname}
                moneys={this.state.moneys}
            ></PresentationalComponent>
        );
    }
}

export default Main;
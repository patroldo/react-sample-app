import React from 'react';
import { StyleSheet, Text, View, TouchableNativeFeedback, Image } from 'react-native';

const PresentationalComponent = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.header}>Hi, {props.nickname}</Text>
            <Text> Your reward points balance is $ {props.moneys}</Text>
            <View>
                <Image source={require('../../assets/Money_bag.png')} />
            </View>
            <TouchableNativeFeedback
                // onPress={props.displayPhoneNumber}   -- uncomment it when finished
                onPress={props.goToCamera}>
                <View
                    style={styles.button}>
                    <Text>Register new coupon</Text>
                </View>
            </TouchableNativeFeedback >
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%'
    },
    header: {
        fontSize: 20
    },
    button: {
        paddingLeft: 50,
        paddingRight: 50,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: 'rgba(255,255,255,0.52)'
    },
});

export default PresentationalComponent;
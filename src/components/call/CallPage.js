import React, { Component } from 'react';
import PresentationalComponent from './PresentationalComponent';
import RNWebsocketServer from 'react-native-custom-websocket-server';
import { DeviceEventEmitter } from 'react-native';
import {
  RTCPeerConnection,
  RTCIceCandidate,
  RTCSessionDescription,
  RTCView,
  MediaStream,
  MediaStreamTrack,
  getUserMedia,
} from 'react-native-webrtc';
import firebase from 'react-native-firebase';
import type { RemoteMessage } from 'react-native-firebase';

class CallPage extends Component {

  state = {
    localStream: null,
    remoteStream: null
  }

  messageListener = null;

  pc = null;
  pc2 = null;
  constructor(props) {
    super(props);
  }

  componentDidMount() {

    const channel = new firebase.notifications.Android.Channel('44F1401633652D5CA2BD34220CBDECFCBC46E859646B61645426057FACC7C923', 'Test Channel', firebase.notifications.Android.Importance.Max)
      .setDescription('My apps test channel');
    firebase.notifications().android.createChannel(channel);
    firebase.messaging().getToken()
      .then((token) => {
        console.log(token);
        firebase.messaging().hasPermission().then(enabled => {
          if (!enabled) {
            firebase.messaging().requestPermission().then(() => {
              this.acceptNewNotification();
            });
          }
          else {
            this.acceptNewNotification();
          }
        })
      });
    let isFront = true;

    configuration = { "iceServers": [{ "url": "stun:stun.l.google.com:19302" }] };
    var pc = new RTCPeerConnection(null);
    var pc2 = new RTCPeerConnection(null);
    MediaStreamTrack.getSources(sourceInfos => {
      console.log(sourceInfos);
      let videoSourceId;
      for (let i = 0; i < sourceInfos.length; i++) {
        const sourceInfo = sourceInfos[i];
        if (sourceInfo.kind == "video" && sourceInfo.facing == (isFront ? "front" : "back")) {
          videoSourceId = sourceInfo.id;
        }
      }
      getUserMedia({
        audio: false,
        video: {
          mandatory: {
            minWidth: 500, // Provide your own width, height and frame rate here
            minHeight: 300,
            minFrameRate: 30
          },
          facingMode: (isFront ? "user" : "environment"),
          optional: (videoSourceId ? [{ sourceId: videoSourceId }] : [])
        }
      }, (stream) => {
        pc2.addStream(stream);
        pc.addStream(stream);

        pc.createOffer((desc) => {
          pc.setLocalDescription(desc, () => {
            // Send pc.localDescription to peer

            this.acceptTheCall(pc2, pc.localDescription, (answear) => {
              pc.setRemoteDescription(
                answear,
                () => {
                },
                (error) => alert(error));
            });
          }, (e) => { });
        }, (e) => { });
        this.setState({
          localStream: stream.toURL()
        });

      }, this.errorMessage);

      pc.onaddstream = (event) => {
        var remoteStream = event.stream;
        this.setState({
          remoteStream: remoteStream.id
        });
      }

      pc.onicecandidate = function (event) {
        // send event.candidate to peer
      }
    });

  }

  acceptNewNotification() {
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      let notificationLocal = new firebase.notifications.Notification()
        .setTitle('not')
        .android.setChannelId('44F1401633652D5CA2BD34220CBDECFCBC46E859646B61645426057FACC7C923')
        .setBody('not')
        .setData({
          key1: 'value1',
          key2: 'value2',
        }
        );
      firebase.notifications().displayNotification(notificationLocal);
      console.log('hi thereFromApp');
    });
    this.messageListener = firebase.messaging().onMessage((message) => {
      console.log(JSON.stringify(message));
    });
    /*this.messageListener = firebase.notifications().onNotification((message) => {
      console.log(JSON.stringify(message));
    });*/
  }

  acceptTheMessage(data) {
    console.log(data);
  }

  errorMessage(data) {
    alert(data);
  }

  componentWillUnmount() {
    this.messageListener();
    this.notificationListener();
  }

  acceptTheCall(pc2, description, callback) {
    pc2.setRemoteDescription(description, () => {
      pc2.createAnswer((answear) => {
        pc2.setLocalDescription(
          answear,
          () => {
            setTimeout(() => callback(pc2.localDescription), 1000)
          },
          (error) => alert(error));
      })
    },
      (error) => {
        alert(error);
      });
  }

  componentWillUnmount() {
  }

  render() {
    return (
      <PresentationalComponent
        localStream={this.state.localStream}
        remoteStream={this.state.remoteStream}
      >
      </PresentationalComponent>
    );
  }
}

export default CallPage;
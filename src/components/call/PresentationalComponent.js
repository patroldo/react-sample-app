import React from 'react';
import { StyleSheet, Dimensions, View, Text } from 'react-native';
import NavBar from './NavBar';
import { RTCView } from 'react-native-webrtc';

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

export default class PresentationalComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.containerNavBar}>
                    <NavBar />
                </View>
                <View>
                    <Text>Call me!!</Text>
                </View>
                <View style={styles.videoContainer}>
                    <RTCView style={{ width: '45%',height:300,backgroundColor:'red' }} streamURL={this.props.remoteStream} />
                    <RTCView style={{ width: '45%',height:300,backgroundColor:'green' }} streamURL={this.props.localStream} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        zIndex: 0
    },
    containerNavBar: {
        position: 'absolute',
        top: 0,
        zIndex: 1,
        width: '100%'
    },
    videoContainer: {
        width: '100%',
        height: 300,
        position: 'relative',
        flex: 1,
        flexDirection: 'row',
        justifyContent:'center'
    }
});
import React, { Component } from 'react';
import PresentationalComponent from './PresentationalComponent';
import { AsyncStorage, Alert } from 'react-native';

class ToDoList extends Component {
    state = {
        toDoList: []
    }
    constructor(props) {
        super(props);
    }

    componentDidMount = async () => {
        try {
            var list = await AsyncStorage.getItem('to_do_list');
            if (list == null) {
                list = JSON.stringify(this.returnStaticArray());
                AsyncStorage.setItem('to_do_list', list).then(() => {
                    this.updateList(list);
                });
            }
            else {
                this.updateList(list);
            }
        }
        catch (err) {
        }
    }

    updateList(list) {
        var readyList = JSON.parse(list);
        this.setState({
            toDoList: readyList
        });
    }

    handleListAdding(value) {
        id = Math.max.apply(Math, this.state.toDoList.map(function (item) { return item.id }), -1);
        id += 1;
        if (value !== null && value !== undefined && value != '') {
        this.addToList(value);
        }
        else {
            Alert.alert("Bad value", "Can't add this string.");
        }
    }

    addToList(value) {
        var newItem = {
            id: id,
            text: value,
            done: false
        }
        this.state.toDoList.push(newItem);
        AsyncStorage.setItem('to_do_list', JSON.stringify(this.state.toDoList)).then(() => {
            this.setState({
                toDoList: this.state.toDoList
            });
        });
    }

    toggle(listItem) {
        item = this.state.toDoList.find((item) => item.id == listItem.id);
        item.done = !item.done;
        AsyncStorage.setItem('to_do_list', JSON.stringify(this.state.toDoList)).then(() => {
            this.setState({
                toDoList: this.state.toDoList
            });
        });
    }

    returnStaticArray() {
        var list = [{
            id: 0,
            text: "Change me ewq",
            done: true
        }, {
            id: 1,
            text: "Change me wqqqsss sssssss sssssssssssss ssssssssssssss sssssss s s sssss ssssss ss ssssss sss sss ss",
            done: true
        }, {
            id: 2,
            text: "Change me qqwe",
            done: false
        }, {
            id: 3,
            text: "Change me qqwq",
            done: true
        }]
        return list;
    }

    onExit() {
        this.props.allowScanning();
    }

    getListItems() {

    }

    render() {
        return (
            <PresentationalComponent
                listItems={this.state.toDoList}
                toggle={(item) => this.toggle(item)}
                addToList={(value) => this.handleListAdding(value)}
            ></PresentationalComponent>
        );
    }
}

export default ToDoList;
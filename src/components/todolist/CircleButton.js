import React from 'react';
import { StyleSheet, View, TouchableNativeFeedback } from 'react-native';
import { Icon } from 'react-native-elements';

const CircleButton = (props) => {
    return (
        <View>
            <TouchableNativeFeedback onPress={props.pressButton}
                background={TouchableNativeFeedback.Ripple('#AAF', true)}>
                <View style={{  
                    padding: (props.navBarHeight / 2 - 15),
                }}>
                    <Icon name={props.iconToDisplay} style={{
                        fontSize: 14,
                        color: 'black',
                    }} />
                </View>
            </TouchableNativeFeedback>
        </View>
    );
}

export default CircleButton;
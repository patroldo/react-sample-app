import React from 'react';
import { StyleSheet, Dimensions, View } from 'react-native';
import { CheckBox } from 'react-native-elements';
import DoneButton from './DoneButton';
import ThrowLineText from './ThrowLineText'
const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

const ListItemElement = (props) => {
    return (
        <View style={styles.listStyle}>
            <View>
                <CheckBox containerStyle={styles.checkboxStyle} checked={props.listItem.done} />
            </View>
            <View style={styles.innerDivText}>
                <ThrowLineText textToShow={props.listItem.text} />
            </View>
            <View style={{ marginRight: 10 }}>
                <DoneButton textToShow={"Done"} press={() => {
                    props.toggle(props.listItem);
                }} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    checkboxStyle: {
        backgroundColor: 'rgba(0,0,0,0)',
        borderWidth: 0,
        paddingRight: 0,
        width: 40
    },
    innerDivText: {
        maxWidth: 0.5 * SCREEN_WIDTH,
        width: 0.5 * SCREEN_WIDTH,
        marginRight: 15
    },
    listStyle: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignItems: 'center',
    }
});

export default ListItemElement;
import React from 'react';
import { StyleSheet, Text, TextInput, View, TouchableNativeFeedback, Alert } from 'react-native';

export default class PopUp extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            text: ''
        }
    }

    okButton() {
        this.props.okClick(this.state.text);
        this.props.closePopUp();
    }

    render() {
        return (
            <View style={styles.popUp}>
                <View style={styles.mainContainer}>
                    <Text style={{ fontSize: 17 }}>Add action to do:</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={(text) => this.setState({ text })}
                        value={this.state.someText}
                    ></TextInput>
                </View>
                <View style={styles.buttons}>
                    <TouchableNativeFeedback onPress={this.props.closePopUp}>
                        <View style={[styles.sungleButton, { marginRight: 85 }]}>
                            <Text style={{ fontSize: 16 }}>Cancel</Text>
                        </View>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback onPress={() => this.okButton()}>
                        <View style={styles.sungleButton}>
                            <Text style={{ fontSize: 16 }}>Add</Text>
                        </View>
                    </TouchableNativeFeedback>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    popUp: {
        backgroundColor: 'rgb(229, 229, 229)',
        flex: 1,
        alignItems: 'center',
        position: 'relative',
    },
    input: {
        backgroundColor: 'white',
        minWidth: '100%',
        marginTop: 12,
        marginBottom: 10,
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 7,
        paddingBottom: 7,
        fontSize: 15
    },
    mainContainer: {
        padding: 8,
        flex: 1,
        alignItems: 'center'
    },
    buttons: {
        width: '100%',
        backgroundColor: 'white',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 8

    },
    sungleButton: {
        backgroundColor: 'rgba(0,0,0,0.1)',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 10,
        paddingTop: 10,
    }
});
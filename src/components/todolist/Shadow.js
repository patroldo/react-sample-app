import React from 'react';
import { View } from 'react-native';

const Shadow = (props) => {

    return (
        <View style={{
            width: props.width,
            height: props.height,
            backgroundColor: 'rgba(0,0,0,0.5)'
        }}></View>
    );
}

export default Shadow; 
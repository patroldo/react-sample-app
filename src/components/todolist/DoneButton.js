import React from 'react';
import { StyleSheet, Text, View,TouchableNativeFeedback } from 'react-native';

const DoneButton = (props) => {
    return (
        <TouchableNativeFeedback
            onPress={props.press}>
            <View
                style={styles.button}>
                <Text>{props.textToShow}</Text>
            </View>
        </TouchableNativeFeedback >
    );
}
const styles = StyleSheet.create({
    button: {
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: 'rgba(235,235,235,0.52)',
    },
});

export default DoneButton;
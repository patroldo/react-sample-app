import React from 'react';
import { StyleSheet, Text } from 'react-native';

export default class DoneButton extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Text style={styles.text}>{this.props.textToShow}</Text>
        );
    }
}

const styles = StyleSheet.create({
    text: {
    },
});
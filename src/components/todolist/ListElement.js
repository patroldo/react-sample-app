import React from 'react';
import { StyleSheet, Dimensions, View } from 'react-native';
import { CheckBox } from 'react-native-elements';
import DoneButton from './DoneButton';
import ThrowLineText from './ThrowLineText'
import PopUp from './PopUp';
import NavBar from './NavBar';
import ListItemElement from './ListItemElement';
const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

const ListElement = (props) => {
    return (
        props.listItems.map((listItem) =>
            <ListItemElement key={listItem.id} listItem={listItem} toggle={props.toggle}/>
        )
    )
}

export default ListElement;
import React from 'react';
import { StyleSheet, Dimensions, View, Animated, ScrollView } from 'react-native';
import PopUp from './PopUp';
import NavBar from './NavBar';
import ListElement from './ListElement';
import Shadow from './Shadow';
const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;
const shadowDuration = 600;
const popupDuration = 600;

export default class PresentationalComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    state = {
        showingPopUp: new Animated.Value(0),  // Initial value for opacity: 0
        showingShadow: new Animated.Value(0),  // Initial value for opacity: 0
        isShowPopUp: false
    }

    showPopUp() {
        this.setState({ isShowPopUp: true });
        Animated.parallel([
            Animated.timing(                  // Animate over time
                this.state.showingPopUp,            // The animated value to drive the line
                {
                    toValue: 1,
                    duration: popupDuration,
                }
            ),
            Animated.timing(
                this.state.showingShadow,            // The animated value to drive a popup
                {
                    toValue: 1,
                    duration: shadowDuration,
                })
        ]).start();                        // Starts the animation
    }

    cancelAnim() {
        Animated.parallel([
            Animated.timing(                  // Animate over time
                this.state.showingPopUp,            // The animated value to drive the line
                {
                    toValue: 0,
                    duration: popupDuration,
                }
            ),
            Animated.timing(
                this.state.showingShadow,            // The animated value to drive a popup
                {
                    toValue: 0,
                    duration: shadowDuration,
                })
        ]).start(() => {
            this.setState({ isShowPopUp: false });
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Animated.View style={[styles.containerShadow, {
                    top: this.state.isShowPopUp ? 0 : -5000,
                    opacity: this.state.showingShadow,
                }]}>
                    <Shadow width={SCREEN_WIDTH} height={SCREEN_HEIGHT} />
                </Animated.View>
                <Animated.View style={[styles.containerPopUp,
                {
                    top: this.state.showingPopUp,
                    transform: [{
                        translateY: this.state.showingPopUp.interpolate({
                            inputRange: [0, 1],
                            outputRange: [-500, 15]
                        }),
                    }]
                },]}
                >
                    <PopUp closePopUp={() => this.cancelAnim()}
                        okClick={this.props.addToList}
                    />
                </Animated.View>
                <View style={styles.containerNavBar}>
                    <NavBar plusClick={() => this.showPopUp()} />
                </View>
                <ScrollView>
                    <View style={
                        {
                            height: '100%'
                        }
                    }>
                        <View style={styles.containerInner}>
                            <ListElement listItems={this.props.listItems} toggle={this.props.toggle} />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        zIndex: 0
    },
    containerShadow: {
        position: 'absolute',
        zIndex: 2
    },
    containerInner: {
        backgroundColor: 'white',
        marginTop:90,
        marginBottom:30
    },
    containerPopUp: {
        position: 'absolute',
        zIndex: 3,
        width: '72%'
    },
    containerNavBar: {
        position: 'absolute',
        top: 0,
        zIndex: 1,
        width: '100%'
    }
});
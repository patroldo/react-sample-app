import {
    View, StyleSheet, StatusBar, TouchableNativeFeedback, Text, Dimensions, Platform
} from 'react-native';
import React, { Component } from 'react';
import { Icon } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import CircleButton from './CircleButton';

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;
const navBarHeight = 64;
const BackButton = Platform.select({
    android: { value: "arrow-back" },
    ios: { value: "ios-arrow-back" },
});


export default class NavBar extends Component {
    render() {
        return (
            <View style={styles.container}>
                <StatusBar />
                <View style={styles.buttons} >
                    <CircleButton 
                        pressButton={() => Actions.pop()}
                        navBarHeight={navBarHeight}
                        iconToDisplay={BackButton.value}/>
                    <CircleButton 
                        pressButton={this.props.plusClick}
                        navBarHeight={navBarHeight}
                        iconToDisplay={"add"}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        height: navBarHeight, 
        ...Platform.select({
            ios: {
                shadowColor: 'black',
                shadowOpacity: 0.8,
                shadowOffset: { width: 2, height: 55 }
            },
            android: {
                elevation: 5
            }
        })
    },
    buttons: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
});
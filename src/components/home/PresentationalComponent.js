import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableNativeFeedback } from 'react-native';

const PresentationalComponent = (props) => {

    return (
        <View style={styles.container}>
            <Text style={styles.header}>Phone Number:</Text>
            <TextInput 
                style={styles.input} 
                onChangeText={(e) => props.changeInput(e)}
                ></TextInput>
            <Text style={styles.header}>Password:</Text>
            <TextInput 
                style={styles.input} 
                onChangeText={(e) => props.changeInput(e)}
                ></TextInput>
            <TouchableNativeFeedback onPress={props.logIn}>
                <View style={styles.button}>
                    <Text style={styles.buttonText}>Submit</Text>
                </View>
            </TouchableNativeFeedback>
            <Text style={styles.errorText}>{props.messageStatus}</Text>
            <Text>You must have an account with elryan.com to be able to use the service</Text>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#4a00ba',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%'
    },
    input: {
        backgroundColor: '#ffffff',
        width: '60%',
        textAlign: 'center',
        fontSize: 15,
        marginBottom: 10
    },
    header: {
        fontSize: 20,
        marginBottom: 10,
        color: '#e5e5e5'
    },
    button: {
        paddingLeft: 50,
        paddingRight: 50,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: 'rgba(255,255,255,0.52)'
    },
    textInButton: {
        fontSize: 20,
        color: '#ffffff'
    },
    errorText: {
        fontSize:12,
        color:'red'
    }
});

export default PresentationalComponent;
import firebase from 'react-native-firebase';

export const backgroundMessageListener = async (message) => {
    console.log('ITSWORK1');
    const notification = new firebase.notifications.Notification().android.setChannelId('44F1401633652D5CA2BD34220CBDECFCBC46E859646B61645426057FACC7C923').setTitle('not').setBody('not');
    // TODO: Configure your notification here...
    notification.android.setCategory(firebase.notifications.Android.Category.Call);
    notification.android.setColorized(true);
    notification.android.setColor('black');
    notification.android.setOngoing(true);
    notification.android.setLights(true);
    // https://rnfirebase.io/docs/v4.3.x/notifications/reference/AndroidAction
    const action = new firebase.notifications.Android.Action('answer', 'ic_launcher', 'answer')
    const action2 = new firebase.notifications.Android.Action('decline', 'ic_launcher', 'decline')
    //notification.android.set
    action.setShowUserInterface(false)
    
    action2.setShowUserInterface(false)
    // https://rnfirebase.io/docs/v4.0.x/notifications/reference/AndroidRemoteInput
    notification.android.addAction(action)
    notification.android.addAction(action2)

    firebase.notifications().displayNotification(notification)
    return Promise.resolve()
}

export const backgroundActionHandler = async (notificationOpen) => {
    if (notificationOpen.action === 'answer') {
        // TODO: Handle the input entered by the user here...
        console.log('answered');
        firebase.notifications().removeAllDeliveredNotifications()
    }
    if (notificationOpen.action === 'decline') {
        // TODO: Handle the input entered by the user here...
        firebase.notifications().removeAllDeliveredNotifications()
        console.log('declined');
    }
    console.log('ITSWORK3');
    return Promise.resolve();
};